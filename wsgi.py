# -*- coding: utf-8 -*-
from werkzeug.wsgi import DispatcherMiddleware

from apps import api, frontend

__author__ = 'alukyanov'


application = DispatcherMiddleware(
    frontend.create_app(),
    {'/API': api.create_app()}
)


if __name__ == "__main__":
    from werkzeug.serving import run_simple

    run_simple('localhost', 8082, application, use_debugger=True,
               use_reloader=True)
