# -*- coding: utf-8 -*-
from apps.api import create_app
from apps.models import db

__author__ = 'alukyanov'


if __name__ == '__main__':
    app = create_app()
    with app.app_context():
        db.drop_all()
        db.create_all()
