import os

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

DEBUG = True
FLASKY_ADMIN = os.environ.get('AC_FLASKY_ADMIN', 'laandrey@gmail.com')

DB_ENGINE = 'postgresql+psycopg2' # more: http://docs.sqlalchemy.org/en/latest/dialects/#included-dialects
DB_USER = 'taxitest'
DB_PASSWORD = 'tak$1'
DB_NAME = 'taxitest2'

# Test whether it solves db bugs:
SQLALCHEMY_POOL_SIZE = 10

SQLALCHEMY_COMMIT_ON_TEARDOWN = True
#SQLALCHEMY_ECHO=True
SECRET_KEY = os.environ.get('SECRET_KEY', 't2$tt2$t')

# Only after local settings
DB_CONNECT_STRING = "{0}:{1}@127.0.0.1:5433/{2}".format(DB_USER, DB_PASSWORD, DB_NAME)
SQLALCHEMY_DATABASE_URI = '{0}://{1}'.format(DB_ENGINE, DB_CONNECT_STRING)