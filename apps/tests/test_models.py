# -*- coding: utf-8 -*-
import json
import sys
sys.path.append('..')
import unittest
from random import randint
from datetime import datetime, timedelta
from flask.ext.testing import TestCase

from ..api import create_app
from ..models import db
from ..models.taxi_dispatcher import Driver, Customer, Order

__author__ = 'alukyanov'


class DispatcherModelsTest(TestCase):

    TESTING = True

    def create_app(self):
        self.app = create_app()
        self.app.config.from_object('apps.settings_test')
        db.init_app(self.app)
        with self.app.app_context():
            db.create_all()
        return self.app

    def setUp(self):
        self.drivers = (
            # id, callname, coords
            (1, '1', '-5.436921,143.614655'),
            (2, '2', '-5.405477,143.629761'),
            (3, '3', '-5.412312,143.659973'),
            (4, '4', '-5.442389,143.687439'),
            (5, '5', '-5.477933,143.702545'),
            (6, '6', '-5.513474,143.698425'),
            (7, '7', '-5.510741,143.66272'),
            (8, '8', '-5.480667,143.632507'),
            (9, '9', '-5.467509,143.617744'),
            (10, '10', '-5.449567,143.611736'),
        )
        for id, callname, coords in self.drivers:
            Driver.create(id=id, callname=callname, coords=coords).save()

        self.customers = (
            # id, phone, name
            (1, '', 'customer 1'),
            (2, '', 'customer 2'),
            (3, '', 'customer 3'),
            (4, '', 'customer 4'),
            (5, '', 'customer 5'),
            (6, '', 'customer 6'),
            (7, '', 'customer 7'),
            (8, '', 'customer 8'),
            (9, '', 'customer 9'),
            (10, '', 'customer 10'),
        )
        for id, phone, name in self.customers:
            Customer.create(phone=phone, name=name).save()

        self.orders = (
            # customer Id, coords, pickup time (None if now)
            (1, '-5.435554,143.616199', None),
            (2, '-5.429402,143.61208', datetime.now() + timedelta(seconds=randint(1, 10))),
            (3, '-5.424275,143.60796', datetime.now() + timedelta(seconds=randint(1, 10))),
            (4, '-5.435041,143.614826', datetime.now() + timedelta(seconds=randint(1, 10))),
            (5, '-5.437263,143.619118', datetime.now() + timedelta(seconds=randint(1, 10))),
            (6, '-5.438801,143.623238', datetime.now() + timedelta(seconds=randint(1, 10))),
            (7, '-5.44051,143.627357', datetime.now() + timedelta(seconds=randint(1, 10))),
            (8, '-5.442389,143.631477', datetime.now() + timedelta(seconds=randint(1, 10))),
            (9, '-5.436237,143.624439', datetime.now() + timedelta(seconds=randint(1, 10))),
            (10, '-5.444953,143.625641', None),
        )
        for id, coords, pickup_time in self.orders:
            Order.create(customer=id, coords=coords,
                         pickup_time=pickup_time).save()


    def test_get_car(self):
        with self.app.test_client() as client:
            res = json.loads(client.get('/Customer/GetCar?id=1').data)
            self.assertTrue(len(res) > 0)

    def test_get_customer(self):
        with self.app.test_client() as client:
            res = json.loads(client.get('/Customer/Info?id=1').data)
            self.assertTrue(res['id'] == 1)

    def test_new_order(self):
        with self.app.test_client() as client:
            data = dict(
                id=2,
                coords='-5.436237,143.624439',
                pickup_time=datetime.now() + timedelta(seconds=randint(1, 10))
            )
            res = json.loads(
                client.post('/Customer/NewOrder', data=data).data)
            self.assertEqual(res['order']['customer_id'], 2)

    def test_new_order_pickup_now(self):
        with self.app.test_client() as client:
            data = dict(
                id=2,
                coords='-5.435554,143.616199',
                pickup_time=None
            )
            res = json.loads(
                client.post('/Customer/NewOrder', data=data).data)
            self.assertEqual(res['order']['customer_id'], 2)
            self.assertNotEqual(res['order']['driver_id'], None)

    def test_cancel_order(self):
        with self.app.test_client() as client:
            data = dict(id=2)
            res = json.loads(
                client.delete('/Customer/Cancel', data=data).data)
            self.assertEqual(res['status'], True)
            order = Order.filter_by(**data).first()
            self.assertNotEqual(order.canceled, None)

    def test_get_driver(self):
        with self.app.test_client() as client:
            res = json.loads(client.get('/Driver/Info?id=1').data)
            self.assertEqual(res['id'], 1)

    def test_set_driver_coords(self):
        with self.app.test_client() as client:
            data = dict(id=2, coords='-5.436237,143.624439')
            res = json.loads(client.put('/Driver/SetCoords', data=data).data)
            self.assertEqual(res['driver']['coords'], data['coords'])

    def tearDown(self):
        db.session.remove()
        db.drop_all()


if __name__ == '__main__':
    unittest.main()
