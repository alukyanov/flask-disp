# -*- coding: utf-8 -*-
import datetime
from flask import Blueprint, request, jsonify

from ..utils import APIError, get_drivers
from ..models.taxi_dispatcher import Customer, Order, Driver

__author__ = 'alukyanov'


customer = Blueprint('customer', __name__, url_prefix='/Customer')


MIN_DISTANCE = 500


@customer.route('/GetCar', methods=['GET'])
def get_car():
    """Returns a JSON-object with list of nearby drivers filtered by customer coords
    :return: JSON: {
        <order Id>: [
            {
                'coords': <coords:str>,
                'dist': <distance in meters:int>,
                'id': <driver Id:int>
            },
            ...
        ],
        ...
    }
    """
    customer_id = request.args.get('id')
    # default distance is 500 meters
    min_distance = request.args.get('min_distance', MIN_DISTANCE)
    customer = Customer.filter_by(id=customer_id).first()
    if customer is None:
        raise APIError('Customer not found', 404)
    customer_orders = Order.filter_by(customer=customer_id, canceled=None,
                                      is_working=False, completed=None).all()
    context = {}
    for order in customer_orders:
        # get nearby drivers
        drivers = [(Driver.filter_by(id=id).first(), dist)
                   for id, dist in get_drivers(order.coords, min_distance)]
        nearby = [d.to_dict(include={'dist': dist}) for d, dist in drivers]
        context[order.id] = nearby
    return jsonify(context)


@customer.route('/Info', methods=['GET'])
def get_customer():
    """Returns a customer info by Id
    :return: JSON: {
        'id': <id:int>,
        'phone': <phone:str>,
        'name': <name:str>,
        'coords': <coords:str>
    }
    """
    customer_id = request.args.get('id')
    customer = Customer.filter_by(id=customer_id).first()
    if customer is None:
        raise APIError('Customer not found', 404)
    return jsonify(customer.to_dict())


@customer.route('/NewOrder', methods=['POST'])
def new_order():
    """New order from customer
    request
    :return: JSON: {
        'id': <id:int>,
        'created': <created:str>,
        'updated': <updated:str>,
        'completed': <completed:str>,
        'canceled': <canceled:str>,
        'is_working': <is_working:bool>,
        'coords': <coords:str>,
        'customer_id': <customer_id:int>,
        'driver_id': <driver_id:int>,
        'description': <description:str>
    }
    """
    customer_id = request.form.get('id')
    phone = request.form.get('phone', '')
    name = request.form.get('name', '')
    coords = request.form.get('coords')
    pickup_time = request.form.get('time') or None

    customer, created = Customer.get_or_create(id=customer_id)
    if created:
        customer.phone = phone
        customer.name = name
        customer.save()

    order, created = Order.get_or_create(
        customer=customer_id, coords=coords, pickup_time=pickup_time)
    if created:
        order.created = datetime.datetime.now()
        order.save()

    if pickup_time is None:
        print pickup_time
        drv = get_drivers(order.coords, MIN_DISTANCE)
        print drv
        if drv:
            order.driver = drv[0][0]
            order.save()
    context = {'order': order.to_dict(), 'created': created}

    return jsonify(context)


@customer.route('/Cancel', methods=['DELETE'])
def cancel_order():
    """Cancel an order by order Id
    :return: JSON: {
        'status': <action status:bool>
    }
    :raises:
        APIError 404 - Order not found
        APIError 409 - Order already canceled
    """
    order_id = request.form.get('id')
    order = Order.filter_by(id=order_id).first()
    if order is None:
        raise APIError('Order not found', 404)
    if order.canceled:
        raise APIError('Order \'{0}\' already canceled'.format(order_id))
    order.canceled = datetime.datetime.now()
    order.save()
    return jsonify({'status': True,
                    'message': 'Order \'{0}\' canceled'.format(order_id)})
