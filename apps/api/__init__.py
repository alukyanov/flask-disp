# -*- coding: utf-8 -*-
from .. import factory
from ..utils import APIError

from flask import jsonify

__author__ = 'alukyanov'


def create_app(name=None):
    """Creates an app, register blueprints and error handlers
    :return: flask app
    """
    app = factory.create_app(name or __name__)

    # registering blueprings
    from .driver import driver
    from .customer import customer

    for bp in driver, customer:
        app.register_blueprint(bp)
        
    #app.json_encoder = JSONEncoder
    app.errorhandler(404)(on_404)
    app.errorhandler(APIError)(on_app_error)

    return app


def on_app_error(e):
    """Response wrapper. Returns a JSON object on view exception.
    :param e:
    :return:
    """
    return jsonify({'error': e.message}), e.status_code


def on_404(e):
    """Default response if requested source is not found
    :param e: exception
    :return: JSON (see docstring of on_app_error function)
    """
    return on_app_error(APIError('Not found', status_code=404))