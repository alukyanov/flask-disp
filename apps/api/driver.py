# -*- coding: utf-8 -*-
from flask import Blueprint, request, jsonify

from ..utils import APIError
from ..models.taxi_dispatcher import Driver

__author__ = 'alukyanov'


driver = Blueprint('driver', __name__, url_prefix='/Driver')


@driver.route('/Info', methods=['GET'])
def get_driver():
    """Returns info about driver by his Id
    :return: JSON: {
        'id': <driver Id:int>,
        'coords': <coords:str>
    }
    :raises:
        APIError 404 - Driver not found
    """
    driver_id = request.args.get('id')
    if driver_id is None:
        raise APIError('Driver ID is not defined')

    driver = Driver.filter_by(callname=driver_id).first()
    if driver is None:
        raise APIError('Driver not found', 404)
    return jsonify(driver.to_dict())


@driver.route('/SetCoords', methods=['PUT'])
def set_driver_coords():
    """Set driver coords
    :return: JSON: {
        'status': <action status:bool>
    }
    """
    driver_id = request.form.get('id')
    coords = request.form.get('coords')
    driver, created = Driver.get_or_create(callname=driver_id)
    driver.coords = coords
    driver.save()
    return jsonify({'status': True, 'driver': driver.to_dict()})
