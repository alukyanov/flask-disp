# -*- coding: utf-8 -*-
from models import db

__author__ = 'alukyanov'


class APIError(Exception):
    """View Exception class
    """
    def __init__(self, message, status_code=400):
        self.message = message
        self.status_code = status_code


def get_drivers(coords, dist):
    """Returns a list of nearby drivers with distance to customer
    :param coords: customer coords
    :param dist: distance to customer
    :return: list: [(<driver Id:int>, <distance in meters:int>), ...]
    """
    lat, lon = coords.split(',')
    SQL = """
        SELECT id, ((
          POINT('{lat}'::FLOAT, '{lon}'::FLOAT) <@>
          POINT(SPLIT_PART(coords, ',', 1)::FLOAT, SPLIT_PART(coords, ',', 2)::FLOAT)
        ) * 1609)::INTEGER
        FROM driver
        WHERE ((
          POINT('{lat}'::FLOAT, '{lon}'::FLOAT) <@>
          POINT(SPLIT_PART(coords, ',', 1)::FLOAT, SPLIT_PART(coords, ',', 2)::FLOAT)
        ) * 1609) <= {dist};
    """.format(dist=dist, lat=lat, lon=lon)
    rows = db.engine.execute(SQL).fetchall()
    return rows
