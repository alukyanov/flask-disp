from flask import render_template
from flask import jsonify

from .. import factory
from ..utils import APIError
from . import assets


def create_app(settings_override=None):
    app = factory.create_app(__name__)
    assets.init_app(app)
    
    from .main import main
    app.register_blueprint(main)

    app.errorhandler(APIError)(on_app_error)
    
    if not app.debug:
        for e in [500, 404]:
            app.errorhandler(e)(handle_error)

    return app


def on_app_error(e):
    return jsonify({'status': e.message}), e.status_code


def handle_error(e):
    return render_template('errors/%s.html' % e.code), e.code