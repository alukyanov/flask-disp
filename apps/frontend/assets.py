from flask.ext.assets import Environment, Bundle


app_css = Bundle(
    'css/bootstrap.css',
    filters="cssmin", output="css/app.min.css")


vendor_js = Bundle(
    'js/lib/jquery.js',
    'js/lib/bootstrap.js',
    filters="jsmin", output="js/lib.min.js")


def init_app(app):
    webassets = Environment(app)
    webassets.register('app_css', app_css)
    webassets.register('vendor_js', vendor_js)
    webassets.manifest = 'cache' if not app.debug else False
    webassets.cache = not app.debug
    webassets.debug = app.debug

