# -*- coding: utf-8 -*-
from flask import Flask
from models import db

__author__ = 'alukyanov'


def create_app(package_name):
    """Creates an Flass app
    :param package_name:
    :return: a[[
    """
    app = Flask(package_name, instance_relative_config=True)
    app.config.from_object('apps.settings')
    db.init_app(app)
    return app