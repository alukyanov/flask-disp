# -*- coding: utf-8 -*-
from datetime import datetime
from . import db, BaseModelMixin

__author__ = 'alukyanov'


class Driver(BaseModelMixin, db.Model):
    """Driver model
    """
    __tablename__ = 'driver'

    id = db.Column(
        db.Integer, primary_key=True, autoincrement=True, nullable=False)
    callname = db.Column(db.String(10), nullable=False, unique=True)
    coords = db.Column(db.String(20))

    def to_dict(self, include=None, exclude=None):
        obj = dict(
            id=self.id,
            callname=self.callname,
            coords=self.coords
        )
        if include:
            obj.update(include)
        return obj


class Customer(BaseModelMixin, db.Model):
    """Customer model
    """
    __tablename__ = 'customer'

    id = db.Column(
        db.Integer, primary_key=True, nullable=False)
    phone = db.Column(db.String(15), nullable=False)
    name = db.Column(db.String(255), nullable=False, unique=True)

    def to_dict(self, include=None, exclude=None):
        return dict(
            id=self.id,
            phone=self.phone,
            name=self.name,
        )


class Order(BaseModelMixin, db.Model):
    """Order model
    """
    __tablename__ = 'order'

    id = db.Column(
        db.Integer, primary_key=True, autoincrement=True, nullable=False)
    created = db.Column(db.DateTime, default=datetime.now)
    updated = db.Column(db.DateTime, default=datetime.now)
    completed = db.Column(db.DateTime)
    canceled = db.Column(db.DateTime)
    is_working = db.Column(db.Boolean, default=False, nullable=False)
    customer = db.Column(db.Integer, db.ForeignKey('customer.id'),
                         nullable=False)
    driver = db.Column(db.Integer, db.ForeignKey('driver.id'))
    coords = db.Column(db.String(20))
    pickup_time = db.Column(db.DateTime)
    description = db.Column(db.Text)

    def to_dict(self, include=None, exclude=None):
        return dict(
            id=self.id,
            created=self.created.isoformat(sep=' '),
            updated=self.updated.isoformat(sep=' '),
            completed=self.completed.isoformat(sep=' ') \
                if self.completed else None,
            canceled=self.canceled.isoformat(sep=' ') \
                if self.canceled else None,
            is_working=self.is_working,
            coords=self.coords,
            customer_id=self.customer,
            driver_id=self.driver,
            description=self.description
        )


