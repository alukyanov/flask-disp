create virtualenv and `pip install -r requirements.txt`
create database `taxitest` and grant access to `taxitest` user
create database `taxitest2` for tests and grant access to `taxitest` user
use PostgreSQL and install extensions before running the tests:
        CREATE EXTENSION cube;
        CREATE EXTENSION earthdistance;
execute `createdb.py` to create database tables
run app `python uwsgi.py`

Testing:
    cd flask_disp
    nosetests

Allowed requests:
    GET /API/Driver/Info
        args:
            